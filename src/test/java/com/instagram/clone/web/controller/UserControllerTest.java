package com.instagram.clone.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.instagram.clone.service.JwtTokenProvider;
import com.instagram.clone.service.UserService;
import com.instagram.clone.web.dto.LoginRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

  @Mock
  private UserService userService;
  @Mock
  private AuthenticationManager authenticationManager;
  @Mock
  private JwtTokenProvider jwtTokenProvider;

  private MockMvc mockMvc;
  private ObjectMapper objectMapper = new ObjectMapper();

  @BeforeEach
  public void beforeEach() {
    mockMvc = MockMvcBuilders.standaloneSetup(new UserController(userService, authenticationManager, jwtTokenProvider))
            .build();
  }

  @Test
  void whenUserIsMakingRequestToLogin_then_testShouldVerifyUserGetsLoggedInSuccessfully() throws Exception {
    var authentication = new UsernamePasswordAuthenticationToken("some user", "some password");
    when(authenticationManager.authenticate(any())).thenReturn(authentication);
    when(jwtTokenProvider.generateToken(any())).thenReturn("dummy_token");

    mockMvc.perform(post("/login")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(LoginRequest.builder().username("some user")
                    .password("some password").build())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.accessToken").value("dummy_token"));
  }

  //@Test
  void whenUserRequestWithIncorrectLoginCredentials_then_testShouldReturnIncorrectCredentialsException() throws Exception {
    var authentication = new UsernamePasswordAuthenticationToken("some user", "some password");
    authentication.setAuthenticated(false);
    when(authenticationManager.authenticate(any())).thenThrow(AuthenticationException.class);

    mockMvc.perform(post("/login")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(LoginRequest.builder().username("some user")
                    .password("some password").build())))
            .andExpect(status().isUnauthorized());
  }
}
