package com.instagram.clone;

import com.instagram.clone.entity.AppUserDetails;
import com.instagram.clone.entity.Profile;
import com.instagram.clone.entity.Role;
import com.instagram.clone.entity.User;

import java.util.HashSet;

public final class TestHelper {

  private TestHelper() {}

  public static User.UserBuilder defaultUser() {
    return User.builder().username("some user").password("some password")
            .userProfile(Profile.builder().displayName("some name").build()).email("someemail@test.com")
            .roles(new HashSet<>(){{ add(Role.USER); }});
  }

  public static AppUserDetails defaultAppUserDetails() {
    return new AppUserDetails(defaultUser().build());
  }
}
