package com.instagram.clone.service;

import com.instagram.clone.TestHelper;
import com.instagram.clone.entity.Profile;
import com.instagram.clone.entity.User;
import com.instagram.clone.repo.UserRepository;
import com.instagram.clone.service.exceptions.EmailAlreadyExistsException;
import com.instagram.clone.service.exceptions.ResourceNotFoundException;
import com.instagram.clone.service.exceptions.UsernameAlreadyExistsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

  @Mock
  private UserRepository userRepository;
  @Mock
  private PasswordEncoder passwordEncoder;

  private UserService userService;

  @BeforeEach
  public void beforeEach() {
    userService = new UserService(userRepository, passwordEncoder);
  }

  @Test
  void whenValidUsernameIsProvided_then_testShouldReturnTheUser() {
    var user = TestHelper.defaultUser().build();
    when(userRepository.findByUsername(any())).thenReturn(Optional.of(user));

    var optionalUser = userService.findByUsername("some user");
    assertThat(optionalUser.isPresent(), is(true));
    assertThat(optionalUser.get().getUsername(), equalTo("some user"));
  }

  @Test
  void whenInvalidUsernameIsProvided_then_testShouldReturnEmptyUser() {
    when(userRepository.findByUsername(any())).thenReturn(Optional.empty());

    var optionalUser = userService.findByUsername("some user");
    assertThat(optionalUser.isPresent(), is(false));
    NoSuchElementException exception = assertThrows(NoSuchElementException.class, () -> optionalUser.get());
    assertThat(exception.getMessage(), equalTo("No value present"));
  }

  @Test
  void whenAskedForAllUser_then_testShouldReturnAllUser() {
    when(userRepository.findAll()).thenReturn(List.of(TestHelper.defaultUser().build()));

    var users = userService.findAll();
    assertThat(users.isEmpty(), is(false));
    assertThat(users.size(), is(1));
    assertThat(users.get(0).getUsername(), equalTo("some user"));
  }

  @Test
  void whenNewUserRegisters_then_testShouldReturnRegisteredUser() {
    when(passwordEncoder.encode(anyString())).thenReturn("dummyEncodedValue");
    when(userRepository.save(any())).thenReturn(TestHelper.defaultUser().password("dummyEncodedValue").build());

    User newUser = TestHelper.defaultUser().roles(null).build();
    User registeredUser = userService.registerUser(newUser);
    assertThat(registeredUser, notNullValue());
    assertThat(registeredUser.getRoles().size(), is(1));
  }

  @Test
  void whenTheUserIsAlreadyRegistered_then_testShouldThrowUsernameAlreadyExistsException() {
    when(userRepository.existsByUsername(any())).thenThrow(UsernameAlreadyExistsException.class);

    User newUser = TestHelper.defaultUser().roles(null).build();
    var exception = assertThrows(UsernameAlreadyExistsException.class, () -> userService.registerUser(newUser));
    assertThat(exception, instanceOf(UsernameAlreadyExistsException.class));
  }

  @Test
  void whenTheUserIsAlreadyRegistered_then_testShouldThrowEmailAlreadyExistsException() {
    when(userRepository.existsByEmail(any())).thenThrow(EmailAlreadyExistsException.class);

    User newUser = TestHelper.defaultUser().roles(null).build();
    var exception = assertThrows(EmailAlreadyExistsException.class, () -> userService.registerUser(newUser));
    assertThat(exception, instanceOf(EmailAlreadyExistsException.class));
  }

  @Test
  void whenUserRequestForProfilePicUpdate_then_testShouldVerifyUserAndUpdateUserProfilePic() {
    var user = TestHelper.defaultUser().id("1").userProfile(Profile.builder().displayName("some name")
            .profilePictureUrl("http://www.oldurl.com").build()).build();
    when(userRepository.findById(any())).thenReturn(Optional.of(user));

    var returnedUser = TestHelper.defaultUser().id("1").userProfile(Profile.builder().displayName("some name")
            .profilePictureUrl("http://www.newurl.com").build()).build();
    when(userRepository.save(any())).thenReturn(returnedUser);
    var actualUser = userService.updateProfilePicture("http://www.newurl.com", "1");
    assertThat(actualUser.getUserProfile().getProfilePictureUrl(), equalTo(returnedUser.getUserProfile()
            .getProfilePictureUrl()));
  }

  @Test
  void whenNonExistingUserRequestForProfilePicUpdate_then_testShouldThrowResourceNotFoundException() {
    when(userRepository.findById(any())).thenThrow(ResourceNotFoundException.class);
    var exception = assertThrows(ResourceNotFoundException.class, () -> userService.updateProfilePicture(
            "http://www.newurl.com","1"));
    assertThat(exception, instanceOf(ResourceNotFoundException.class));
  }
}
