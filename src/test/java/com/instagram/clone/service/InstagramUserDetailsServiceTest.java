package com.instagram.clone.service;

import com.instagram.clone.TestHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class InstagramUserDetailsServiceTest {

  @Mock
  private UserService userService;
  private InstagramUserDetailsService instagramUserDetailsService;

  @BeforeEach
  public void beforeEach() {
    instagramUserDetailsService = new InstagramUserDetailsService(userService);
  }

  @Test
  void whenValidUsernameIsGiven_then_testShouldReturnUserDetail() {
    var user = TestHelper.defaultAppUserDetails();
    when(userService.findByUsername(any())).thenReturn(Optional.of(user));

    var userDetails = instagramUserDetailsService.loadUserByUsername("some user");
    assertThat(userDetails, notNullValue());
    assertThat(userDetails.getUsername(), equalTo("some user"));
  }

  @Test
  void whenInvalidUserNameIsGiven_then_testShouldThrowUsernameNotFoundException() {
    when(userService.findByUsername(any())).thenThrow(UsernameNotFoundException.class);
    UsernameNotFoundException exception = assertThrows(UsernameNotFoundException.class,
            () -> instagramUserDetailsService.loadUserByUsername("some user"));
    assertThat(exception, notNullValue());
  }
}
