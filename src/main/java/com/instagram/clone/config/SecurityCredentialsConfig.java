package com.instagram.clone.config;

import com.instagram.clone.entity.Role;
import com.instagram.clone.service.JwtTokenProvider;
import com.instagram.clone.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityCredentialsConfig extends WebSecurityConfigurerAdapter {

  private final String serviceUserName;
  private final String servicePassword;

  private final JwtConfig jwtConfig;
  private final JwtTokenProvider jwtTokenProvider;
  @Autowired
  private UserService userService;
  @Autowired
  private UserDetailsService userDetailsService;

  public SecurityCredentialsConfig(@Value("${security.service.username}") final String serviceUserName,
                                   @Value("${security.service.password}") final String servicePassword,
                                   final JwtConfig jwtConfig, final JwtTokenProvider jwtTokenProvider) {
    this.serviceUserName = serviceUserName;
    this.servicePassword = servicePassword;
    this.jwtConfig = jwtConfig;
    this.jwtTokenProvider = jwtTokenProvider;
  }

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http.csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .exceptionHandling().authenticationEntryPoint((req, res, e) -> res.sendError(SC_UNAUTHORIZED))
            .and()
            .addFilterBefore(new JwtTokenAuthenticationFilter(serviceUserName, jwtConfig, jwtTokenProvider, userService),
                    UsernamePasswordAuthenticationFilter.class)
            .authorizeRequests()
            .antMatchers(HttpMethod.POST, "/login").permitAll()
            .antMatchers(HttpMethod.POST, "/users").anonymous()
            .anyRequest().authenticated();
  }

  @Override
  protected void configure(final AuthenticationManagerBuilder authBuilder) throws Exception {
    authBuilder.inMemoryAuthentication()
            .withUser(serviceUserName)
            .password(passwordEncoder().encode(servicePassword))
            .roles(Role.SERVICE.getName());
    authBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
  }

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean(BeanIds.AUTHENTICATION_MANAGER)
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }
}
