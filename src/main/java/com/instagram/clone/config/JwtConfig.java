package com.instagram.clone.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class JwtConfig {

  private final String uri;
  private final String header;
  private final String prefix;
  private final int expiration;
  private final String secret;

  public JwtConfig(@Value("${security.jwt.uri}") final String uri,
                   @Value("${security.jwt.header}") final String header,
                   @Value("${security.jwt.prefix:Bearer }") final String prefix,
                   @Value("${security.jwt.expiration:#{24*60*60}}") final int expiration,
                   @Value("${security.jwt.secret}") final String secret) {
    this.uri = uri;
    this.header = header;
    this.prefix = prefix;
    this.expiration = expiration;
    this.secret = secret;
  }
}
