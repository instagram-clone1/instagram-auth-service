package com.instagram.clone.config;

import com.instagram.clone.entity.AppUserDetails;
import com.instagram.clone.service.JwtTokenProvider;
import com.instagram.clone.service.UserService;
import io.jsonwebtoken.Claims;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {

  private final String serviceUsername;
  private final JwtConfig jwtConfig;
  private final JwtTokenProvider jwtTokenProvider;
  private final UserService userService;

  public JwtTokenAuthenticationFilter(final String serviceUsername, final JwtConfig jwtConfig,
                                      final JwtTokenProvider jwtTokenProvider, final UserService userService) {
    this.serviceUsername = serviceUsername;
    this.jwtConfig = jwtConfig;
    this.jwtTokenProvider = jwtTokenProvider;
    this.userService = userService;
  }

  @Override
  protected void doFilterInternal(final HttpServletRequest req, final HttpServletResponse res,
                                  final FilterChain chain) throws ServletException, IOException {
    final String header = req.getHeader(jwtConfig.getHeader());
    if (header == null || !header.startsWith(jwtConfig.getPrefix())) {
      chain.doFilter(req, res);
      return;
    }
    final String token = header.replace(jwtConfig.getPrefix(), "");
    jwtTokenProvider.validateToken(token).ifPresentOrElse(val -> {
      Claims claims = jwtTokenProvider.getClaimsFromJwt(token);
      String username = claims.getSubject();
      UsernamePasswordAuthenticationToken authToken = getUsernamePasswordAuthToken(username, claims, req);
      SecurityContextHolder.getContext().setAuthentication(authToken);
    }, () -> SecurityContextHolder.clearContext());
    chain.doFilter(req, res);
  }

  private UsernamePasswordAuthenticationToken getUsernamePasswordAuthToken(final String username,
                                                                           final Claims claims,
                                                                           final HttpServletRequest req) {
    if (username.equalsIgnoreCase(serviceUsername)) {
      List<String> authorities = (List<String>) claims.get("authorities");
      return new UsernamePasswordAuthenticationToken(username, null, authorities.stream()
              .map(SimpleGrantedAuthority::new).collect(toList()));
    }
    return userService.findByUsername(username)
            .map(AppUserDetails::new).map(userDetail -> buildAuthToken(userDetail, req)).orElse(null);
  }

  private UsernamePasswordAuthenticationToken buildAuthToken(final AppUserDetails userDetails,
                                                             final HttpServletRequest req) {
    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
            userDetails, null, userDetails.getAuthorities());
    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
    return authentication;
  }
}
