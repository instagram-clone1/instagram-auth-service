package com.instagram.clone.service;

import com.instagram.clone.entity.Role;
import com.instagram.clone.entity.User;
import com.instagram.clone.repo.UserRepository;
import com.instagram.clone.service.exceptions.EmailAlreadyExistsException;
import com.instagram.clone.service.exceptions.ResourceNotFoundException;
import com.instagram.clone.service.exceptions.UsernameAlreadyExistsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class UserService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  public UserService(final UserRepository userRepository, final PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.passwordEncoder = passwordEncoder;
  }

  public Optional<User> findByUsername(final String username) {
    log.info("retrieving user {]", username);
    return userRepository.findByUsername(username);
  }

  public List<User> findAll() {
    log.info("retrieving all users");
    return userRepository.findAll();
  }

  public List<User> findByUsernameIn(final Set<String> usernames) {
    return userRepository.findByUsernameIn(usernames);
  }

  public User registerUser(final User user) {
    log.info("registering user {}", user.getUsername());
    checkAndVerifyUser(user);
    User activeUser = user.toBuilder().active(true).password(passwordEncoder.encode(user.getPassword()))
            .roles(new HashSet<>() {{ add(Role.USER);}}).build();
    return userRepository.save(activeUser);
  }

  public User updateProfilePicture(final String uri, final String id) {
    log.info("update profile picture {} for user {}", uri, id);
    return userRepository.findById(id).map(user -> updateUserProfile(user, uri))
            .orElseThrow(() -> new ResourceNotFoundException(String.format("user id %s not found", id)));
  }

  private User updateUserProfile(final User user, final String uri) {
    final String oldProfileUrl = user.getUserProfile().getProfilePictureUrl();
    var userProfile = user.getUserProfile();
    userProfile = userProfile.toBuilder().profilePictureUrl(uri).build();
    var updatedUser = user.toBuilder().userProfile(userProfile).build();
    return userRepository.save(updatedUser);
  }

  private void checkAndVerifyUser(final User user) {
    if (userRepository.existsByUsername(user.getUsername())) {
      log.warn("username {} already exists", user.getUsername());
      throw new UsernameAlreadyExistsException(String.format("username %s already exists", user.getUsername()));
    }
    if (userRepository.existsByEmail(user.getEmail())) {
      log.warn("email {} already exists", user.getEmail());
      throw new EmailAlreadyExistsException(String.format("email %s already exists", user.getEmail()));
    }
  }
}
