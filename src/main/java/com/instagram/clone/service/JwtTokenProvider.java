package com.instagram.clone.service;

import com.instagram.clone.config.JwtConfig;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class JwtTokenProvider {

  public final JwtConfig jwtConfig;

  public JwtTokenProvider(final JwtConfig jwtConfig) {
    this.jwtConfig = jwtConfig;
  }

  public String generateToken(final Authentication authentication) {
    Long now = System.currentTimeMillis();
    return Jwts.builder()
            .setSubject(authentication.getName())
            .claim("authorities", authentication.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority).collect(toList()))
            .setIssuedAt(new Date(now))
            .setExpiration(new Date(now + jwtConfig.getExpiration() * 1000))
            .signWith(HS256, jwtConfig.getSecret().getBytes()).compact();
  }

  public Claims getClaimsFromJwt(final String token) {
    return Jwts.parser().setSigningKey(jwtConfig.getSecret().getBytes()).parseClaimsJws(token)
            .getBody();
  }

  public Optional<String> validateToken(final String authToken) {
    try {
      Jwts.parser().setSigningKey(jwtConfig.getSecret().getBytes()).parseClaimsJws(authToken);
      return Optional.of("Valid");
    } catch (SignatureException | MalformedJwtException | ExpiredJwtException |
            UnsupportedJwtException | IllegalArgumentException exception) {
      log.error("Unsupported JWT token");
    }
    return Optional.empty();
  }
}
