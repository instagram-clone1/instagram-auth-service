package com.instagram.clone.service.exceptions;

public class EmailAlreadyExistsException extends RuntimeException {

  public EmailAlreadyExistsException(final String message) {
    super(message);
  }
}
