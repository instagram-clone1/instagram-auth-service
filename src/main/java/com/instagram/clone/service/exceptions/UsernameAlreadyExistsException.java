package com.instagram.clone.service.exceptions;

public class UsernameAlreadyExistsException extends RuntimeException {

  public UsernameAlreadyExistsException(final String message) {
    super(message);
  }
}
