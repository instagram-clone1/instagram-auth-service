package com.instagram.clone.service;

import com.instagram.clone.entity.AppUserDetails;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class InstagramUserDetailsService implements UserDetailsService {

  private final UserService userService;

  public InstagramUserDetailsService(final UserService userService) {
    this.userService = userService;
  }

  @Override
  public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
    return userService.findByUsername(username).map(AppUserDetails::new)
            .orElseThrow(() -> new UsernameNotFoundException("Username not found"));
  }
}
