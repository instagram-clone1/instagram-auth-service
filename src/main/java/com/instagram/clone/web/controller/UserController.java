package com.instagram.clone.web.controller;

import com.instagram.clone.entity.AppUserDetails;
import com.instagram.clone.entity.Profile;
import com.instagram.clone.entity.User;
import com.instagram.clone.service.JwtTokenProvider;
import com.instagram.clone.service.UserService;
import com.instagram.clone.service.exceptions.ResourceNotFoundException;
import com.instagram.clone.web.dto.ApiResponse;
import com.instagram.clone.web.dto.JwtAuthenticationResponse;
import com.instagram.clone.web.dto.LoginRequest;
import com.instagram.clone.web.dto.SignUpRequest;
import com.instagram.clone.web.dto.UserSummary;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@Slf4j
@RestController
public class UserController {

  private final UserService userService;
  private final AuthenticationManager authenticationManager;
  private final JwtTokenProvider jwtTokenProvider;

  public UserController(final UserService userService, final AuthenticationManager authenticationManager,
                        final JwtTokenProvider jwtTokenProvider) {
    this.userService = userService;
    this.authenticationManager = authenticationManager;
    this.jwtTokenProvider = jwtTokenProvider;
  }

  @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody final LoginRequest loginRequest) {
    Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    var token = jwtTokenProvider.generateToken(authentication);
    return ResponseEntity.ok(new JwtAuthenticationResponse(token));
  }

  @PostMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createUser(@Valid @RequestBody final SignUpRequest signUpRequest) {
    log.info("creating user {}", signUpRequest.getUsername());
    var user = User.builder().username(signUpRequest.getUsername()).email(signUpRequest.getEmail())
            .password(signUpRequest.getPassword()).userProfile(Profile.builder().displayName(signUpRequest.getName())
                    .build()).build();
    userService.registerUser(user);
    URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{username}")
            .buildAndExpand(user.getUsername()).toUri();
    return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
  }

  @PutMapping("/users/me/picture")
  @PreAuthorize("hasRole('USER')")
  public ResponseEntity<?> updateProfilePicture(@RequestBody final String profilePicture,
                                                @AuthenticationPrincipal AppUserDetails userDetails) {
    userService.updateProfilePicture(profilePicture, userDetails.getId());
    return ResponseEntity.ok().body(new ApiResponse(true, "Profile picture updated successfully"));
  }

  @GetMapping(value = "/users/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findUser(@PathVariable("username") final String username) {
    log.info("retrieving user {}", username);
    return userService.findByUsername(username).map(user -> ResponseEntity.ok(user))
            .orElseThrow(() -> new ResourceNotFoundException(username));
  }

  @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findAll() {
    log.info("retrieving all users");
    return ResponseEntity.ok(userService.findAll());
  }

  @GetMapping(value = "/users/me", produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('USER')")
  public ResponseEntity<UserSummary> getLoggedInUser(@AuthenticationPrincipal final AppUserDetails userDetails) {
    return ResponseEntity.ok(mapUserToSummary(userDetails));
  }

  @PostMapping(value = "/users/summary/in", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> getUserSummaries(@RequestBody final Set<String> usernames) {
    log.info("retrieving summaries for {} usernames", usernames.size());
    var summaries = userService.findByUsernameIn(usernames).stream().map(this::mapUserToSummary).collect(toList());
    return ResponseEntity.ok(summaries);
  }

  private UserSummary mapUserToSummary(final User user) {
    return UserSummary.builder().id(user.getId()).username(user.getUsername())
            .displayName(user.getUserProfile().getDisplayName())
            .profilePicUrl(user.getUserProfile().getProfilePictureUrl()).build();
  }
}
