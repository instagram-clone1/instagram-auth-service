package com.instagram.clone.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class UserSummary {

  private final String id;
  private final String username;
  private final String displayName;
  private final String profilePicUrl;
}
