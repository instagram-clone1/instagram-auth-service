package com.instagram.clone.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
@AllArgsConstructor
public class JwtAuthenticationResponse {

  @NonNull
  private final String accessToken;
}
