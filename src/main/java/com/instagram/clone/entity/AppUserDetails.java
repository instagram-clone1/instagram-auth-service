package com.instagram.clone.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

import static java.util.stream.Collectors.toSet;

public class AppUserDetails extends User implements UserDetails {

  public AppUserDetails(final User user) {
    super(user);
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return getRoles().stream().map(role -> new SimpleGrantedAuthority("ROLE_" + role.getName()))
            .collect(toSet());
  }

  @Override
  public boolean isAccountNonExpired() {
    return isActive();
  }

  @Override
  public boolean isAccountNonLocked() {
    return isActive();
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return isActive();
  }

  @Override
  public boolean isEnabled() {
    return isActive();
  }
}
