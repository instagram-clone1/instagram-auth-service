package com.instagram.clone.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Address {

  private String id;
  private String country;
  private String city;
  private String zipCode;
  private String streetName;
  private int buildingNumber;
}
