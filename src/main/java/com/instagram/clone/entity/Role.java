package com.instagram.clone.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class Role {

  public final static Role USER = new Role("USER");
  public final static Role SERVICE = new Role("SERVICE");

  private final String name;
}
