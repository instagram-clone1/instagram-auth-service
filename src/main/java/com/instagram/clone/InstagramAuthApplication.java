package com.instagram.clone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication
@EnableMongoAuditing
public class InstagramAuthApplication {

  public static void main(String[] args) {
    SpringApplication.run(InstagramAuthApplication.class, args);
  }
}
