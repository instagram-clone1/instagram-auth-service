package com.instagram.clone.repo;

import com.instagram.clone.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserRepository extends MongoRepository<User, String> {

  Optional<User> findByUsername(final String username);
  List<User> findByUsernameIn(final Set<String> usernames);
  boolean existsByUsername(final String username);
  boolean existsByEmail(final String email);
}
